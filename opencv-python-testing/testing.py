import numpy as np
import timeit


def sum_up_to_n(n):
    res = 0
    for i in range(1, n + 1):
        res += i
    return res


def sumup(n):
    return sum(range(1, n + 1))


sumup_lam = lambda n: sum(range(1, n + 1))

sumup_list = lambda n: sum(x for x in range(1, n + 1))


def sum_up_to_np(n):
    return np.sum(np.arange(1, n + 1))


def main():
    n = 1000
    tests = 100_000

    result = timeit.timeit(lambda: sum_up_to_n(n), number=tests)
    print(f"Without numpy: {result} seconds")

    result = timeit.timeit(lambda: sum_up_to_np(n), number=tests)
    print(f"   With numpy: {result} seconds")

    result = timeit.timeit(lambda: sumup(n), number=tests)
    print(f" sum_up smart: {result} seconds")

    result = timeit.timeit(lambda: sumup_lam(n), number=tests)
    print(f"  sum_up lam1: {result} seconds")

    result = timeit.timeit(lambda: sumup_list(n), number=tests)
    print(f"  sum_up lam2: {result} seconds")


if __name__ == "__main__":
    main()