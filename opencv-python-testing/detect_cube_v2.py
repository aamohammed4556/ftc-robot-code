
import cv2
import numpy as np

cap = cv2.VideoCapture(0)
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))
lower_orange = np.array([10,130,40])
upper_orange = np.array([50,255,255])
BRIGHTNESS = 40

def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV_FULL)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def draw_img_rect(result, c, i):
  x,y,w,h = cv2.boundingRect(c)
  cv2.rectangle(result, (x, y), (x+w, y+h), (255, 144, 0), 2)
  cv2.putText(result,f"{i + 1}", (x+2,y+25), cv2.FONT_ITALIC, 1, 255)


while 1:  # 727
    # Take each frame
    _, frame = cap.read()
    
    
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    blurred = increase_brightness(blurred, value=BRIGHTNESS)
    # Convert BGR to HSV 
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV_FULL)    

    # Threshold the HSV image to get only certain colors
    orange_mask = cv2.inRange(hsv, lower_orange, upper_orange)

    # Bitwise-AND mask and original image
    img = cv2.bitwise_and(blurred, blurred, mask=orange_mask) 
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # threshold
    thresh = cv2.threshold(gray,10, 255,cv2.THRESH_BINARY)[1]

    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=3)   

    result = frame
    # get contours
    contours = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    if len(contours) != 0:
      contours = sorted(contours, key = cv2.contourArea, reverse=True)
      for i, c in enumerate(contours):
        if i <= 2: 
          draw_img_rect(result, c, i)

      
    cv2.imshow('Depth Perception', result)
    cv2.imshow('opening to reduce noise', opening)
    #cv2.imshow('thresh', thresh)
  
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

cv2.destroyAllWindows()
cap.release()
